package com.mistno.networkdiscover;

import java.io.*;
import java.net.*;

public class NetworkDiscoverListener {

	private static final int LISTENING_PORT = 14976;

	public static void main(String[] args) {
		try{
			ServerSocket server = new ServerSocket(LISTENING_PORT);
			System.out.println("Starting to listen for scanner");
			Socket socket = server.accept();

			PrintWriter pw = new PrintWriter(new OutputStreamWriter(socket.getOutputStream()));
			pw.write("Found you!");
			pw.flush();
			pw.close();
			socket.close();
			server.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}