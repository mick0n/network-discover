package com.mistno.networkdiscover;

import java.io.*;
import java.net.*;
import java.nio.CharBuffer;
import java.util.concurrent.*;

/**
 * Used to scan a C-class subnet for a corresponding NetworkDiscoverListener
 */
public class NetworkDiscoverScanner {

	private static final int SCAN_PORT = 14976;

	public static void main(String[] args) {
		if (args.length != 1) {
			System.out.println("Usage: java NetworkDiscoverScanner 192.168.1.0");
			System.exit(0);
		}

		ExecutorService service = Executors.newFixedThreadPool(16);

		String ipPrefix = args[0].substring(0, args[0].lastIndexOf(".") + 1);
		for (int i = 1; i <= 255; i++) {
			final int suffix = i;
			service.submit(() -> {
				try {
					Socket socket = new Socket();
					socket.connect(new InetSocketAddress(ipPrefix + suffix, SCAN_PORT), 1000);
					CharBuffer charBuffer = CharBuffer.allocate(1024);
					InputStreamReader isr = new InputStreamReader(socket.getInputStream());
					isr.read(charBuffer);
					charBuffer.rewind();
					System.out.println(charBuffer.toString() + ": " + ipPrefix + suffix);
					socket.close();
					System.exit(0);
				} catch (IOException e) {
					System.err.println("Wasn't " + ipPrefix + suffix);
				}
			});
		}
		service.shutdown();
	}
}